/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package polynomial;
//We need the class monoms in order to create a Polynom
//Each Polynom is made from an ArrayList of Monoms
public class Monoms {
	protected Number coefficient;
	protected int degree;
	public Monoms()
	{
		this.coefficient = 0;
		this.degree = 0;
	}
	public Monoms(Number coefficient, int degree)
	{
		this.coefficient = coefficient;
		this.degree = degree;
	}
	public void setCoefficient(Number x)
	{
		this.coefficient = x;
	}
	public void setDegree(int x)
	{
		this.degree = x;
	}
	public Number getCoefficient()
	{
		return this.coefficient;
	}
	public int getDegree()
	{
		return this.degree;
	}
}
