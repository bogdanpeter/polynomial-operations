/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package polynomial;

import java.util.List;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

public class Polynomial {
	private List<Monoms> terms= new ArrayList<Monoms>();
	private int degree;

	public Polynomial()
	{
		this.degree = 0;
	}
	
	public Polynomial(List<Monoms> terms, int degree)
	{
		this.terms = terms;
		this.degree = degree;
	}
	
	public Polynomial(String input) // we create a Polynom from a string
	{
		input = input.replaceAll(Pattern.quote(" "), ""); // first we get rid of any empty spaces
		input = input.replaceAll(Pattern.quote("*"), "");
		input = input.replaceAll(Pattern.quote("^"), " ");// we replace '^' with a space so that we can 
		input = input.replaceAll("X", "x");// have the coefficients before x and the degree after x
		String[] inputS = input.split("(?=\\+)|(?=\\-)"); // we split the string when we find +/-
		for (String t : inputS)
		{
			Monoms temp = new MonomsInt();	
			if (t.indexOf("x") != -1) { // we verify if the splitted string contains x
				if (t.startsWith("x"))
					t = "1" + t; // if the splitted string starts with x we add a '1' in front
				else if ((t.startsWith("+") || t.startsWith("-")) && t.charAt(1) == 'x') {
					String aux = t.substring(0, 1) + "1" + t.substring(1, t.length());
					t = aux;  // if we have +x/-x we add a '1' between +/- and x
				}
				temp.setCoefficient(Integer.parseInt(t.substring(0, t.indexOf("x"))));
				// we check if there is something after x
				if (t.substring(t.indexOf("x") + 1, t.length()).length() > 1) 	
					temp.setDegree(Integer.parseInt(t.substring(t.indexOf("x") + 2, t.length())));
				else
					temp.setDegree(1);  //if there is nothing after x we make the degree 1
				this.terms.add(temp);
			} else 
			{
				temp.setCoefficient(Integer.parseInt(t));
				temp.setDegree(0);  // if the splitted string doesn't contain x we make the degree 0
				this.terms.add(temp);
			}
		}
	}
			 
		 
	
	
	public void addMonom(Number coefficient, int degree)
	{
		Monoms newTerm = new Monoms(coefficient, degree);
		this.terms.add(newTerm);
		this.setDegree();
	}
	
	public void addMonom(Monoms monom)
	{
		this.terms.add(monom);
	}
	
	public void removeZero() // removes the monoms that have the coefficient equal to 0
	{
		Iterator<Monoms> i = this.terms.iterator();
		
		while(i.hasNext())
		{
			Monoms k = i.next();
			if(k.getCoefficient().doubleValue() == 0)
				i.remove();
		}
		
		
	}
	
	public void clear() 
	{
		this.terms.clear();
		this.degree = 0;
	}
	
	public void addSameTerms() // adds the coefficients from the monoms that have the same degree
	{				
		Iterator<Monoms> i = this.terms.iterator();
		while(i.hasNext())
		{
			Monoms terms1 = i.next();
			Iterator<Monoms> j= this.terms.iterator();
			while(j.hasNext())
			{
				Monoms terms2 = j.next();
				if(terms1 != terms2)
				{
					if(terms1.getDegree() == terms2.getDegree())
					{	// if the 2 terms have the same degree we add the coefficients in the first one and set the second one to 0
						terms1.setCoefficient(terms1.getCoefficient().doubleValue() + terms2.getCoefficient().doubleValue());
						terms2.setCoefficient(0);
						terms2.setDegree(0);
					}
				}
			}
		}
		this.removeZero(); // we remove all the terms that were set to 0
		this.setDegree();
		
	}
	
	public void sort() // sorts the monoms by degree from biggest to smallest
	{
		Iterator<Monoms> i = this.terms.iterator();
		int k=0;
		while(i.hasNext())
		{
			k++;
			Monoms aux = new Monoms();
			Monoms sort1 = i.next();
			Iterator<Monoms> j = this.terms.iterator();
			int l=0;
			while(j.hasNext())
			{
				l++;
				Monoms sort2 = j.next();
				if(l>k && sort1.getDegree() < sort2.getDegree())
				{
					aux.setCoefficient(sort2.getCoefficient().doubleValue());
					aux.setDegree(sort2.getDegree());
					sort2.setCoefficient(sort1.getCoefficient().doubleValue());
					sort2.setDegree(sort1.getDegree());
					sort1.setCoefficient(aux.getCoefficient().doubleValue());
					sort1.setDegree(aux.getDegree());
				}
			}
		}
	}
	
	public void setDegree() // sets the degree of the polynomial to the highest monom degree
	{
		this.degree = 0;
		Iterator<Monoms> i = this.getMonoms().iterator();
		while(i.hasNext())
		{
			Monoms aux = i.next();
			if(aux.getDegree() > degree)
				this.degree = aux.getDegree();
			
		}
	}
	
	public String print() // returns a string with the polynomial
	{
		this.sort();
		Iterator<Monoms> i = this.getMonoms().iterator();
		Monoms aux = new Monoms();
		StringBuilder output = new StringBuilder();
		while(i.hasNext())
		{
			aux = i.next();
			// if the coefficient is pozitive and its not the first term of the Polynom we add a +
			if ((aux.getCoefficient().doubleValue()>0) && (aux.getDegree() != this.getDegree())) output.append('+');
			// if the coefficient is -1 and the degree is 0 we add a -
			else if((aux.getCoefficient().doubleValue() == -1) && (aux.getDegree() != 0)) output.append('-');
			if(((aux.getCoefficient().doubleValue()!= 1) && (aux.getCoefficient().doubleValue()!= -1)) || (aux.getDegree() == 0))
			{
			/* we format the output to have max 2 decimals. we need to print doublevalue because
				we can get a double number wen we divide/integrate polynomials*/
				output.append(new DecimalFormat("#.##").format(aux.getCoefficient().doubleValue()));
			}
			if (aux.getDegree() > 1)  // if the degree is grater than 1 we also add 'x^'
			{
				output.append("X^");
				output.append(aux.getDegree());
			}
			else if (aux.getDegree() != 0) output.append('X'); // if the degree is equal to 1 we print an
		}												// x without '^'
	if(output.length() == 0) output.append('0');
	return output.toString();
	}
	
	public List<Monoms> getMonoms()
	{
		return this.terms;
	}
	
	public int getDegree()
	{
		this.setDegree(); // we make sure that the Degree is set first
		return this.degree;
	}
	
}
