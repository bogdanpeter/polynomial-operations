/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package polynomial;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Operations {
	
	public static Polynomial add(Polynomial x, Polynomial y)
	{
		Polynomial rezult = new Polynomial();
		Iterator<Monoms> i = x.getMonoms().iterator();
		while(i.hasNext())		// Add all the terms from x in the rezult polynomial
		{
			Monoms xMonom = i.next();
			rezult.addMonom(xMonom.getCoefficient().intValue(), xMonom.getDegree());
		}
		Iterator<Monoms> j = y.getMonoms().iterator();
		while(j.hasNext())		// Add all the terms from y in the rezult polynomial
		{
			Monoms yMonom = j.next();
			rezult.addMonom(yMonom.getCoefficient().intValue(), yMonom.getDegree());
		}
		rezult.addSameTerms(); // this function adds the terms with the same Degree
		rezult.sort();
		return rezult;
	}
	
	public static Polynomial subtract(Polynomial x, Polynomial y)
	{
		Polynomial rezult = new Polynomial();
		Iterator<Monoms> i = x.getMonoms().iterator();
		while(i.hasNext())		// Add all the terms from x
		{
			Monoms xMonom = i.next();
			rezult.addMonom(xMonom.getCoefficient().doubleValue(), xMonom.getDegree());
		}
		Iterator<Monoms> j = y.getMonoms().iterator();
		while(j.hasNext())		// Add all the terms from y with -
		{
			Monoms yMonom = j.next();
			rezult.addMonom(-(yMonom.getCoefficient().doubleValue()), yMonom.getDegree());
		}
		rezult.addSameTerms();// this function adds the terms with the same Degree
		rezult.sort();
		return rezult;
	}
	
	public static Polynomial multiply(Polynomial x, Polynomial y)
	{
		Polynomial rezult = new Polynomial();
		Iterator<Monoms> i = x.getMonoms().iterator();
		while(i.hasNext())		
		{
			Monoms xMonom = i.next();
			Iterator<Monoms> j = y.getMonoms().iterator();
			while(j.hasNext())
			{
				Monoms yMonom = j.next();
				rezult.addMonom(xMonom.getCoefficient().doubleValue() * yMonom.getCoefficient().doubleValue(), xMonom.getDegree() + yMonom.getDegree());		
			}	// we create the monoms for rezult by multiplying every monom from x
			   //	with every monom from y
		}
		rezult.addSameTerms();  
		rezult.sort();
		return rezult;

	}
	
	public static Polynomial differentiate(Polynomial x)
	{
		Polynomial rezult = new Polynomial();
		Iterator<Monoms> i = x.getMonoms().iterator();
		while(i.hasNext())
		{
			Monoms xMonom = i.next();
			rezult.addMonom(xMonom.getCoefficient().intValue() * xMonom.getDegree(), xMonom.getDegree()-1);
		}	//we create the rezult monoms by differentiating every monom in x according to the formula
		rezult.addSameTerms();
		return rezult;
	}
	
	public static Polynomial integrate(Polynomial x)
	{
		Polynomial rezult = new Polynomial();
		Iterator<Monoms> i = x.getMonoms().iterator();
		while(i.hasNext())
		{
			Monoms xMonom = i.next();
			rezult.addMonom(xMonom.getCoefficient().doubleValue() / (xMonom.getDegree()+1), xMonom.getDegree()+1);
		}//we create the rezult monoms by integrating every monom in x according to the formula
		rezult.addSameTerms();
		rezult.sort();
	
		return rezult;
	}
	
	public static List<Polynomial> division(Polynomial x, Polynomial y)
	{	
		Polynomial rezult = new Polynomial();
		Polynomial intermediar = new Polynomial();
		y.removeZero(); // we remove the terms with coefficient 0 from y to avoid division by 0
		while((x.getDegree() >= y.getDegree()) && (!x.getMonoms().isEmpty()))
		{		
			intermediar.clear();
			Monoms xMonom = x.getMonoms().get(0);
			Monoms yMonom = y.getMonoms().get(0);
			/* we divide the leading terms and add them to rezult & intermediar */
			rezult.addMonom(xMonom.getCoefficient().doubleValue() / yMonom.getCoefficient().doubleValue(), xMonom.getDegree() - yMonom.getDegree());
			intermediar.addMonom(xMonom.getCoefficient().doubleValue() / yMonom.getCoefficient().doubleValue(), xMonom.getDegree() - yMonom.getDegree());
			intermediar= Operations.multiply(intermediar, y); // we multiply the intermediar with y and
			x = Operations.subtract(x, intermediar);		//  then subtract it from x so that
		}													// in x remains the rest
		rezult.removeZero();
		rezult.addSameTerms();
		rezult.sort();
		List<Polynomial> ret = new ArrayList<Polynomial>(); //we create an array and return
		ret.add(rezult);										// the rezult at position 0
		ret.add(x);												//and the rest at position 1
		return ret;
	}
}
