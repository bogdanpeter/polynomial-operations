/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package polynomial;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class View extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JTextField inputTf1 = new JTextField(45);
	private JTextField inputTf2 = new JTextField(45);
	private JTextField rezultTf = new JTextField(45);
	private JButton addBtn = new JButton("Add (f+g)");
	private JButton subtractBtn = new JButton("Subtract (f-g)");
	private JButton multiplyBtn = new JButton("Multiply (f*g)");
	private JButton differentiateBtn = new JButton("Differentiate (f)");
	private JButton integrateBtn = new JButton("Integrate (f)");
	private JButton divideBtn = new JButton("Divide (f/g)");
	private Font font =new Font(Font.MONOSPACED,10,20);

	// we create the interface
	View()
	{
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setBounds(0, 0, 800, 500);
		this.setTitle("Polynomial Calculator");
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 800, 500);
		
		JLabel inputL1 = new JLabel("First  Polynomial[f]:");
		inputL1.setBounds(40, 70, 290, 25);
		inputL1.setForeground(Color.red);
		inputL1.setFont(font);

		inputTf1.setBounds(300,70,450, 30);
		inputTf2.setBounds(300,110,450,30);
		panel.add(inputL1);
		panel.add(inputTf1);
		panel.add(inputTf2);
		JLabel inputL2 = new JLabel("Second Polynomial[g]:");
		inputL2.setBounds(40, 110, 290, 25);
		inputL2.setForeground(Color.RED);
		inputL2.setFont(font);
		panel.add(inputL2);
		
		addBtn.setBounds(40, 180, 120, 70);
		panel.add(addBtn);
		subtractBtn.setBounds(160, 180, 120, 70);
		panel.add(subtractBtn);
		multiplyBtn.setBounds(280, 180, 120, 70);
		panel.add(multiplyBtn);
		differentiateBtn.setBounds(400,180,120,70);
		panel.add(differentiateBtn);
		integrateBtn.setBounds(520, 180, 120, 70);
		panel.add(integrateBtn);
		divideBtn.setBounds(640, 180, 120, 70);
		panel.add(divideBtn);
		
		JLabel rezultL = new JLabel("Rezult:");
		rezultL.setBounds(350,330,150,20);
		rezultL.setForeground(Color.RED);
		rezultL.setFont(font);
		panel.add(rezultL);
		rezultTf.setText("");
		rezultTf.setEditable(false);
		rezultTf.setBounds(125, 350, 550, 30);
		panel.add(rezultTf);
		this.setVisible(true);
		this.setContentPane(panel);
	
	}
	
	String getUserInput1()
	{
		return inputTf1.getText();
	}
	String getUserInput2()
	{
		return inputTf2.getText();
	}
	void setRezult(String newRezult)
	{
		rezultTf.setText(newRezult);
	}
	void showError(String errMessage) {
		JOptionPane.showMessageDialog(this, errMessage);
		}
	void addMultiplyListener(ActionListener m) {
		multiplyBtn.addActionListener(m);
		}
	void addAddListener(ActionListener a)
	{
		addBtn.addActionListener(a);
	}
	void addDifferentiateListener(ActionListener d)
	{
		differentiateBtn.addActionListener(d);
	}
	void addDivisionListener(ActionListener d)
	{
		divideBtn.addActionListener(d);
	}
	void addIntegrateListener(ActionListener i)
	{
		integrateBtn.addActionListener(i);
	}
	void addSubtractListener(ActionListener s)
	{
		subtractBtn.addActionListener(s);
	}
}