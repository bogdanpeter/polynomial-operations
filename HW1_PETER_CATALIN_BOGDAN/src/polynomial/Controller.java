/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */

package polynomial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;





// We create a controller to implement what operations
// each button does
public class Controller{
	
	private View view;
	
	Controller(View view)
	{
		this.view = view;
		view.addAddListener(new AddListener());
		view.addDifferentiateListener(new DifferentiateListener());
		view.addDivisionListener(new DivisionListener());
		view.addIntegrateListener(new IntegrateListener());
		view.addMultiplyListener(new MultiplyListener());
		view.addSubtractListener(new SubtractListener());	
	}
	class MultiplyListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
		try {
			Polynomial input1 =new Polynomial(view.getUserInput1());
			Polynomial input2 =new Polynomial(view.getUserInput2());
			Polynomial rezult=Operations.multiply(input1, input2);
			view.setRezult(rezult.print());
		} catch (NumberFormatException nfex) {
			view.showError("Bad input! \n Input format:  coefficientX^degree");		}
		}
	}
	class AddListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Polynomial input1 =new Polynomial(view.getUserInput1());
				Polynomial input2 =new Polynomial(view.getUserInput2());
				Polynomial rezult=Operations.add(input1, input2);
				view.setRezult(rezult.print());
			} catch (NumberFormatException nfex) {
				view.showError("Bad input! \n Input format:  coefficientX^degree");		}
			}
	}
	class SubtractListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Polynomial input1 =new Polynomial(view.getUserInput1());
				Polynomial input2 =new Polynomial(view.getUserInput2());
				Polynomial rezult=Operations.subtract(input1, input2);
				view.setRezult(rezult.print());
			} catch (NumberFormatException nfex) {
				view.showError("Bad input! \n Input format:  coefficientX^rade");		}
			}
	}
	class IntegrateListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Polynomial input1 =new Polynomial(view.getUserInput1());
				Polynomial rezult=Operations.integrate(input1);
				view.setRezult(rezult.print());
			} catch (NumberFormatException nfex) {
				view.showError("Bad input! \n Input format:  coefficientX^degree");		}
			}
	}
	class DifferentiateListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Polynomial input1 =new Polynomial(view.getUserInput1());
				Polynomial rezult=Operations.differentiate(input1);
				view.setRezult(rezult.print());
			} catch (NumberFormatException nfex) {
				view.showError("Bad input! \n Input format:  coefficientX^degree");		
				}
			}
			}
	class DivisionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				Polynomial input1 =new Polynomial(view.getUserInput1());
				Polynomial input2 =new Polynomial(view.getUserInput2());
				try {
					List<Polynomial> rezult=Operations.division(input1, input2);
					view.setRezult(rezult.get(0).print() + "  Rest: " + rezult.get(1).print());
				}
				catch(Exception exc)
				{
					view.showError("Bad input!(Division by 0)");
				}
				
			} catch (Exception nfex) {
				view.showError("Bad input! \n Input format:  coefficientX^degree");		}
			}
	}
}
			


