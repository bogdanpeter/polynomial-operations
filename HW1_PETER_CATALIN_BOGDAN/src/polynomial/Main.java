/*
	Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package polynomial;


// The main class that starts the Interface
public class Main {
	public static void main(String[] args) {	
		View       view       = new View();
		Controller controller = new Controller(view);
}
}