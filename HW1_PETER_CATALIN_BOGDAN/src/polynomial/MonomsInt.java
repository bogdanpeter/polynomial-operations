/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package polynomial;

public class MonomsInt extends Monoms {
	
	MonomsInt()
	{
		super(0,0);
	}
	MonomsInt(int coefficient, int degree)
	{
		super(coefficient,degree);
	}

}
