/*
 * Homework1 Polynomial Operations
 * Name: Peter Catalin-Bogdan
 * Group:30423
 * 
 */
package testing;
import polynomial.Polynomial;
import polynomial.Operations;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PolynomialTest {
Polynomial x = new Polynomial();
Polynomial y = new Polynomial();
	@Before
	public void setValues(){
		x = new Polynomial("4x^3 +3x^2+4");
		y = new Polynomial("x^2+2");
	}
	@Test
	public void testAddition(){
		System.out.println("Addition:");
		setValues();
		Polynomial adunare = Operations.add(x, y);
		Polynomial expected = new Polynomial("4X^3+4X^2+6");
		System.out.println("rezult:" + adunare.print());
		System.out.println("expected:"+ expected.print());
		assertEquals(adunare.print(),expected.print());
	}
	@Test
	public void testSubtraction(){
		System.out.println("Subtraction:");
		setValues();
		Polynomial scadere = Operations.subtract(x, y);
		Polynomial expected = new Polynomial("4X^3+2X^2+2");
		System.out.println("rezult:" + scadere.print());
		System.out.println("expected:"+ expected.print());
		assertEquals(scadere.print(), expected.print());
	}
	@Test
	public void testMultiplication(){
		System.out.println("Multiplication:");
		setValues();
		Polynomial multiplication = Operations.multiply(x, y);
		Polynomial expected = new Polynomial("4X^5+3X^4+8X^3+10X^2+8");
		System.out.println("rezult:" + multiplication.print());
		System.out.println("expected:"+ expected.print());
		assertEquals(multiplication.print(), expected.print());
	}
	@Test
	public void testDifferentiate(){
		System.out.println("Differentiate:");
		setValues();
		Polynomial derivare = Operations.differentiate(x);
		Polynomial expected = new Polynomial("12X^2+6X");
		System.out.println("rezult:" + derivare.print());
		System.out.println("expected:"+ expected.print());
		assertEquals(derivare.print(), expected.print());
	}
	@Test
	public void testDivision(){
		System.out.println("Division:");
		setValues();
		List<Polynomial> impartire = Operations.division(x, y);
		List<Polynomial> expected = new ArrayList<Polynomial>();
		expected.add(new Polynomial("4X+3"));
		expected.add(new Polynomial("-8X-2"));
		System.out.println("rezult:" + impartire.get(0).print() + " rest:" + impartire.get(1).print());
		System.out.println("expected:"+ expected.get(0).print()+ "rest: " + expected.get(1).print());
		assertEquals(expected.get(0).print()+expected.get(1).print(),impartire.get(0).print()+impartire.get(1).print());
	}
	@Test
	public void testIntegrate(){
		System.out.println("Integrate:");
		setValues();
		Polynomial integrala = Operations.integrate(x);
		Polynomial expected = new Polynomial("X^4+X^3+4X");
		System.out.println("rezult:" + integrala.print());
		System.out.println("expected:"+ expected.print());
		assertEquals(integrala.print(), expected.print());
	}

}
